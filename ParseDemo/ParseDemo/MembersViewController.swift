//
//  MemebrsViewController.swift
//  ParseDemo
//
//  Created by Akkshay Khoslaa on 10/21/15.
//  Copyright © 2015 Akkshay Khoslaa. All rights reserved.
//

import UIKit


class MembersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MemberTableViewCellDelegate {
    
    var memberNames = Array<String>()
    //Create an array to store the profile pictures 
    /* YOUR CODE HERE */

    @IBOutlet weak var membersTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationController!.navigationBar.hidden = false
        self.navigationController!.navigationBar.barTintColor = UIColor(red: 0.2, green: 0.678, blue: 1, alpha: 1)
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        self.navigationController!.navigationBar.titleTextAttributes = titleDict as? Dictionary
        self.navigationController!.navigationBar.tintColor = UIColor.whiteColor()
        membersTableView.delegate = self
        membersTableView.dataSource = self
        getMembers()
        getProfPics()
        membersTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getMembers() {
        // Get the list of all the members' names and add them to the memberNames array. Then reload the tableview.
        /* YOUR CODE HERE */
    }
    
    func getProfPics() {
        // Get the profile pictures of all the members and add them to the profPics array. Then reload the tableview.
        /* YOUR CODE HERE */
    }
    
    
    func hit(cell: MemberTableViewCell) {
        // Update the member's hits field in the database.
        /* YOUR CODE HERE */
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return memberNames.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("memberCell", forIndexPath: indexPath) as! MemberTableViewCell
        cell.memberProfilePic.layer.cornerRadius = cell.memberProfilePic.frame.size.width/2
        cell.memberProfilePic.clipsToBounds = true
        cell.delegate = self
        
        
        // Set the text of the memberName field of the cell to the right value
        
        
        // Set the image of the memberProfilePic imageview in the cell
      
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
        
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
