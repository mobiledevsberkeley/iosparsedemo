//
//  SpotlightViewController.swift
//  ParseDemo
//
//  Created by Akkshay Khoslaa on 10/21/15.
//  Copyright © 2015 Akkshay Khoslaa. All rights reserved.
//

import UIKit

class SpotlightViewController: UIViewController {

    @IBOutlet weak var memberProfPic: UIImageView!
    @IBOutlet weak var memberName: UILabel!
    @IBOutlet weak var memberHits: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.navigationBar.hidden = false
        self.navigationController!.navigationBar.barTintColor = UIColor(red: 0.2, green: 0.678, blue: 1, alpha: 1)
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        self.navigationController!.navigationBar.titleTextAttributes = titleDict as? Dictionary
        self.navigationController!.navigationBar.tintColor = UIColor.whiteColor()
        memberProfPic.layer.cornerRadius = memberProfPic.frame.size.width/2
        memberProfPic.clipsToBounds = true
        getMemberInfo()
        // Do any additional setup after loading the view.
    }
    
    func getMemberInfo() {
        // Write query to get the info of the user with the most hits. Then set the respective fields' values.
        /* YOUR CODE HERE */
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
